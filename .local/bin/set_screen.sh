# Usage:
#	set_screen.sh widthxheight
#	(default is 1024x768)

output1=eDP1
output2=VIRTUAL1

wxh=$1

if [ -z "$wxh" ]; then
    wxh=1024x768
fi

ivo_process=`ps axu | grep 'intel-virtual-output' | egrep -v 'grep'`
if [ -z "$ivo_process" ]; then
    intel-virtual-output
    sleep 3
fi

output1_mode=1920x1080
echo Using mode for $output1: $output1_mode

output2_mode=`xrandr | grep $output2\\\. | grep $wxh | awk '{print $1}'`
output2_mode=`echo $output2_mode | awk '{print $1}'`

echo Using mode for $output2: $output2_mode

# xrandr --output $output1 --mode $output1_mode --output $output2 --mode $output2_mode --same-as $output1
xrandr --output $output1 --mode $output1_mode --output $output2 --mode $output2_mode
xrandr --output $output2 --right-of $output1
xrandr --output $output1 --primary
