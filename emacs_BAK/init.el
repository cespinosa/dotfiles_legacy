(eval-when-compile
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)
  (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
  (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package)
    (package-install 'diminish)
    (package-install 'quelpa)
    (package-install 'bind-key)
    )
  (setq use-package-always-ensure t)
  (setq use-package-expand-minimally t)
  
  (require 'use-package)
  )

(require 'diminish)
(require 'bind-key)

(use-package server
 :ensure nil
 :hook (after-init . server-mode)
 )

(setq inhibit-startup-screen t)
(setq inhibit-startup-message t)
(setq inhibit-startup-echo-area-message t)
(setq initial-scratch-message nil)

(if window-system
    (progn
      ;; UI parts
      (toggle-scroll-bar 0)
      (tool-bar-mode 0)
;;      (menu-bar-mode 0)
      
      ;; Overwrite latin and greek char's font
      (defun set-latin-and-greek-font (family)
        (set-fontset-font (frame-parameter nil 'font) '(#x0250 . #x02AF) (font-spec :family family)) ; IPA extensions
        (set-fontset-font (frame-parameter nil 'font) '(#x00A0 . #x00FF) (font-spec :family family)) ; latin-1
        (set-fontset-font (frame-parameter nil 'font) '(#x0100 . #x017F) (font-spec :family family)) ; latin extended-A
        (set-fontset-font (frame-parameter nil 'font) '(#x0180 . #x024F) (font-spec :family family)) ; latin extended-B
        (set-fontset-font (frame-parameter nil 'font) '(#x2018 . #x2019) (font-spec :family family)) ; end quote
        (set-fontset-font (frame-parameter nil 'font) '(#x2588 . #x2588) (font-spec :family family)) ; █
        (set-fontset-font (frame-parameter nil 'font) '(#x2500 . #x2500) (font-spec :family family)) ; ─
        (set-fontset-font (frame-parameter nil 'font) '(#x2504 . #x257F) (font-spec :family family)) ; box character
        (set-fontset-font (frame-parameter nil 'font) '(#x0370 . #x03FF) (font-spec :family family))
      	)

      (setq use-default-font-for-symbols nil)
      (setq inhibit-compacting-font-caches t)
      (setq default-font-family "FuraCode Nerd Font")

      ;; (set-face-attribute 'default nil :family default-font-family)
      (set-face-attribute 'default nil :family default-font-family :height 120)

      (set-latin-and-greek-font default-font-family)
      (add-to-list 'face-font-rescale-alist (cons default-font-family 1.00))
      )
  )

(setq-default frame-title-format
              '(:eval
                (format "%s@%s: %s %s"
                        (or (file-remote-p default-directory 'user)
                            user-real-login-name)
                        (or (file-remote-p default-directory 'host)
                            system-name)
                        (buffer-name)
                        (cond
                         (buffer-file-truename
                          (concat "(" buffer-file-truename ")"))
                         (dired-directory
                          (concat "{" dired-directory "}"))
                         (t
                          "[no file]")))))
(setq ring-bell-function 'ignore)

(use-package smartparens
  :hook
  (after-init . smartparens-global-mode)
  :config
  (require 'smartparens-config)
  (sp-pair "=" "=" :actions '(wrap))
  (sp-pair "+" "+" :actions '(wrap))
  (sp-pair "<" ">" :actions '(wrap))
  (sp-pair "$" "$" :actions '(wrap))
  )

(use-package all-the-icons
  :defer t
  )

(use-package git-timemachine
  :bind ("M-g t" . git-timemachine-toggle)
  )

(use-package magit
  :custom
  (magit-auto-revert-mode nil)
  :bind
  ("M-g s" . magit-status)
  )

(use-package tex
  :defer t
  :ensure auctex
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq TeX-save-query nil)
  (add-to-list 'TeX-view-program-selection
                 '(output-pdf "Zathura"))
)

(use-package org
    :custom
    (org-agenda-files (file-expand-wildcards "~/Google_Drive/fractaliusfciencias/orgmode/*.org"))
)

(use-package doom-themes
  :custom
  (doom-themes-enable-italic t)
  (doom-themes-enable-bold t)
  :config
  (load-theme 'doom-dracula t)
  (doom-themes-org-config)
  ;; Modeline
  (use-package doom-modeline
    :hook
    (after-init . doom-modeline-mode)
    )
  )

(use-package nyan-mode
  :custom
  (nyan-cat-face-number 4)
  (nyan-animate-nyancat t)
  :hook
  (doom-modeline-mode . nyan-mode)
  )

(use-package rainbow-delimiters
  :hook
  (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :diminish
  :hook (emacs-lisp-mode . rainbow-mode))

(use-package evil
  :ensure t
  :defer .1
  :config
  (evil-mode 1)
  (use-package evil-org
    :ensure t
    :config
    (evil-org-set-key-theme
      '(textobjects insert navigation additional shift todo heading))
    (add-hook 'org-mode-hook (lambda () (evil-org-mode))
               )
    (require 'evil-org-agenda)
    (evil-org-agenda-set-keys)
  )
)

(setq display-line-numbers-type 'visual)
(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))
;;(setq display-line-numbers-mode 'relative)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(doom-themes-enable-bold t)
 '(doom-themes-enable-italic t)
 '(magit-auto-revert-mode nil)
 '(org-agenda-files
   (quote
    ("~/Google_Drive/fractaliusfciencias/orgmode/HIIregions_CALIFA_physical_properties_paper.org" "~/Google_Drive/fractaliusfciencias/orgmode/pyGHIIexplorer.org")))
 '(package-selected-packages (quote (evil-org quelpa diminish use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
