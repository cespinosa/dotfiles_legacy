#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#
# Exports
#

# Adds `~/.local/bin` to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export JUPYTER_CONFIG_DIR="$HOME/.config/jupyter"
export CALIFA_DATA="/home/espinosa/Google_Drive/cespinosa/CALIFA/data/"
export DATAFILES="/home/espinosa/Google_Drive/cespinosa/data/"

# export TERMINAL="st"
export TERMINAL="alacritty"
export READER="zathura"
export EDITOR="nvim"

#
# Alias
#

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME' 
alias open='xdg-open &>/dev/null'
alias soledad='ssh cespinosa@soledad.astroscu.unam.mx -p 8006'
alias manga_IA="ssh -X manga@10.0.0.161"
alias manga_out="ssh -X -p 8017 manga@132.248.1.15"
alias taranis="ssh -X -p 22418 carlos@3mdb.astro.unam.mx"
alias conda_powers="source /home/espinosa/conda_init.sh"
alias ipy='ipython --pylab'
alias jupylab='jupyter-lab'

PATH="/home/espinosa/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/espinosa/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/espinosa/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/espinosa/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/espinosa/perl5"; export PERL_MM_OPT;

