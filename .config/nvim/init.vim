let mapleader =","

if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))

Plug 'morhetz/gruvbox'
Plug 'tpope/vim-fugitive'
" Plug 'jreybert/vimagit'
Plug 'preservim/nerdtree'
Plug 'preservim/nerdcommenter'
Plug 'ajh17/VimCompletesMe'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py'}
" Plug 'frazrepo/vim-rainbow'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'  " Temas para airline
Plug 'Yggdroot/indentLine'
Plug 'rbong/pimodoro'
"Plug 'jupyter-vim/jupyter-vim'
"Plug 'szymonmaszke/vimpyter'
" Plug 'sirver/ultisnips'
" Plug 'honza/vim-snippets'
" Plug 'easymotion/vim-easymotion'
" Plug 'numirias/semshi'

call plug#end()

" Some basics:
	colorscheme gruvbox
	syntax on
	set encoding=utf-8
	set noerrorbells
	set tabstop=4 softtabstop=4
	set shiftwidth=4
	set expandtab
	set smartcase
	set noswapfile
	set nobackup
	set undodir=~/.vim/undodir
	set undofile
	set incsearch
	set number relativenumber
	set cursorline
	set cursorcolumn
	set colorcolumn=80
	highlight ColorColumn ctermbg=0 guibg=lightgrey

" Pimodoro Config
    :let g:pim#taskfile = '/home/espinosa/Google_Drive/fractaliusfciencias/taskfile'

" Nerd tree
	map <C-n> :NERDTreeToggle<CR>

" VimCompleteMe
    inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Airline
	let g:airline_theme='luna'
	let g:airline#extensions#tabline#enabled = 1  " Mostrar buffers abiertos (como pestañas)
	let g:airline#extensions#tabline#fnamemod = ':t'  " Mostrar sólo el nombre del archivo
	" Cargar fuente Powerline y símbolos (ver nota)
	let g:airline_powerline_fonts = 1
	set noshowmode  " No mostrar el modo actual (ya lo muestra la barra de estado)

" indentLine
	let g:indentLine_color_term = 256 
	" No mostrar en ciertos tipos de buffers y archivos
	let g:indentLine_fileTypeExclude = ['text', 'sh', 'help', 'terminal']
	let g:indentLine_bufNameExclude = ['NERD_tree.*', 'term:.*']

" ultisnips
	" Expandir snippet con Ctrl + j
	" let g:UltiSnipsExpandTrigger = '<c-j>'
	" Ir a siguiente ubicacion con Ctrl + j
	" let g:UltiSnipsJumpForwardTrigger = '<c-j>'
	" Ir a anterior ubicacion con Ctrl + k
	" let g:UltiSnipsJumpBackwardTrigger = '<c-k>'

" easymotion
	" map <Leader> <Plug>(easymotion-prefix)
